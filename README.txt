After Migration from 6 to 7,
the users cannot login with the existing password,
they have to reset password in order to login in.

The following module will give access to users using the old password.
